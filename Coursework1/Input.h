#pragma once

#include <Windows.h>
#include <map>

class Input
{
public:
	Input();
	~Input();
	
	/// <summary>Returns whether the specified key was pressed this frame.</summary>
	/// <param name='keyCode'>The key code for the key to check.</param>
	bool GetKeyDown(UINT keyCode);
	/// <summary>Returns whether the specified key was released this frame.</summary>
	/// <param name='keyCode'>The key code for the key to check.</param>
	bool GetKeyUp(UINT keyCode);

	/// <summary>Processes a Windows keyboard message.</summary>
	/// <param name='message'>The message to process.</summary>
	/// <returns><c>true</c> if the message was processed; otherwise, <c>false</c>.</returns>
	bool ProcessKeyboardMessage(MSG message);

	/// <summary>Flushes the keyboard messages.</summary>
	void FlushKeyboardMessages();

private:
	std::map<UINT, bool> _keysDown;
	std::map<UINT, bool> _keysUp;
};

