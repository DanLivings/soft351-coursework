#pragma once
#include "GraphicsSettings.h"
#include "DirectXGraphicsSettings.h"
#include "Mesh.h"
#include <map>
#include <list>
#include <memory>

enum GraphicsApi { ApiDirectX, ApiOpenGL };
enum ShaderType { Vertex, Pixel };

class Graphics
{
private:
	class DirectXGraphics;
public:
	/// <summary>Creates a <see cref='Graphics' /> instance that uses the specified graphics API.</summary>
	/// <param name='windowHandle'>The handle to the window.</param>
	/// <param name='graphicsApi'>The graphics API to use.</param>
	/// <returns>A new instance of the <see cref='Graphics' /> class.</returns>
	static Graphics* CreateGraphicsObject(HWND windowHandle, GraphicsApi graphicsApi);
	/// <summary>Clears the renderer.</summary>
	virtual void Clear() = 0;
	/// <summary>Loads a pixel shader from a file with the specified name.</summary>
	/// <param name='shaderName'>The name of the shader file excluding the extension.</summary>
	virtual void LoadPixelShader(std::string shaderName) = 0;
	/// <summary>Loads a vertex shader from a file with the specified name.</summary>
	/// <param name='shaderName'>The name of the shader file excluding the extension.</summary>
	virtual void LoadVertexShader(std::string shaderName) = 0;
	/// <summary>Renders the current frame.</summary>
	virtual void Render() = 0;
	/// <summary>Renders a <see cref='Mesh' /> to the current frame.</summary>
	/// <param name='mesh'>The mesh to render.</param>
	virtual void RenderMesh(Mesh* mesh) = 0;
	/// <summary>Sets the specified texture map with the given data.</summary>
	/// <param name='textureIndex'>The texture slot to set.</param>
	/// <param name='texture'>The texture to use.</param>
	virtual void SetTextureMap(UINT textureIndex, Texture* texture) = 0;
	/// <summary>Sets the projection matrix for the renderer.</summary>
	/// <param name='projectionMatrix'>The projection matrix.</param>
	virtual void SetProjectionMatrix(DirectX::XMMATRIX projectionMatrix) = 0;
	/// <summary>Sets the view matrix for the renderer.</summary>
	/// <param name='viewMatrix'>The view matrix.</param>
	virtual void SetViewMatrix(DirectX::XMMATRIX viewMatrix) = 0;
	/// <summary>Sets the world matrix for the renderer.</summary>
	/// <param name='worldMatrix'>The world matrix.</param>
	virtual void SetWorldMatrix(DirectX::XMMATRIX worldMatrix) = 0;
	/// <summary>Sets the current pixel shader to that with the specified name.</summary>
	/// <param name='shaderName'>The name of the shader to use.</param>
	virtual void SetPixelShader(std::string shaderName) = 0;
	/// <summary>Sets the current vertex shader to that with the specified name.</summary>
	/// <param name='shaderName'>The name of the shader to use.</param>
	virtual void SetVertexShader(std::string shaderName) = 0;
	/// <summary>Updates the specified constant buffer with the given data.</summary>
	/// <param name='shaderType'>A value specifying whether the constant buffer belongs to the vertex or pixel shader.</param>
	/// <param name='bufferIndex'>The buffer slot to update. Currently unused.</param>
	/// <param name='data'>The data to insert into the buffer.</param>
	virtual void UpdateConstantBuffer(ShaderType shaderType, UINT bufferIndex, void* data) = 0;
	/// <summary>Finalizes the <see cref='Graphics' /> instance.</summary> 
	virtual ~Graphics() = 0;
protected:
	/// <summary>Initializes the graphics settings.</summary>
	virtual void InitializeGraphicsSettings() = 0;
	/// <summary>Initializes a new instance of the <see cref='Graphics' /> class.</summary>
	Graphics();

	DirectX::XMMATRIX _projectionMatrix;
	DirectX::XMMATRIX _viewMatrix;
	DirectX::XMMATRIX _worldMatrix;
};

class Graphics::DirectXGraphics :
	public Graphics
{
public:
	/// <summary>Initializes a new instance of the <see cref='DirectXGraphics' />
	/// class using the specified window handle.</summary>
	/// <param name='windowHandle'>The handle to the window.</param>
	DirectXGraphics(HWND windowHandle);
	/// <summary>Finalizes the <see cref='DirectXGraphics' /> instance.</summary> 
	~DirectXGraphics();
	/// <summary>Clears the renderer.</summary>
	void Clear() override;
	/// <summary>Loads a pixel shader from a file with the specified name.</summary>
	/// <param name='shaderName'>The name of the shader file excluding the extension.</param>
	void LoadPixelShader(std::string shaderName) override;
	/// <summary>Loads a vertex shader from a file with the specified name.</summary>
	/// <param name='shaderName'>The name of the shader file excluding the extension.</param>
	void LoadVertexShader(std::string shaderName) override;
	/// <summary>Renders the current frame.</summary>
	void Render() override;
	/// <summary>Renders a <see cref='Mesh' /> to the current frame.</summary>
	/// <param name='mesh'>The mesh to render.</param>
	void RenderMesh(Mesh* mesh) override;
	/// <summary>Sets the specified texture map with the given data.</summary>
	/// <param name='textureIndex'>The texture slot to set.</param>
	/// <param name='texture'>The texture to use.</param>
	void SetTextureMap(UINT textureIndex, Texture* texture) override;
	/// <summary>Sets the projection matrix for the renderer.</summary>
	/// <param name='projectionMatrix'>The projection matrix.</param>
	void SetProjectionMatrix(DirectX::XMMATRIX projectionMatrix) override;
	/// <summary>Sets the view matrix for the renderer.</summary>
	/// <param name='viewMatrix'>The view matrix.</param>
	void SetViewMatrix(DirectX::XMMATRIX viewMatrix) override;
	/// <summary>Sets the world matrix for the renderer.</summary>
	/// <param name='worldMatrix'>The world matrix.</param>
	void SetWorldMatrix(DirectX::XMMATRIX worldMatrix) override;
	/// <summary>Sets the current pixel shader to that with the specified name.</summary>
	/// <param name='shaderName'>The name of the shader to use.</param>
	void SetPixelShader(std::string shaderName) override;
	/// <summary>Sets the current vertex shader to that with the specified name.</summary>
	/// <param name='shaderName'>The name of the shader to use.</param>
	void SetVertexShader(std::string shaderName) override;
	/// <summary>Updates the specified constant buffer with the given data.</summary>
	/// <param name='shaderType'>A value specifying whether the constant buffer belongs to the vertex or pixel shader.</param>
	/// <param name='bufferIndex'>The buffer slot to update. Currently unused.</param>
	/// <param name='data'>The data to insert into the buffer.</param>
	void UpdateConstantBuffer(ShaderType shaderType, UINT bufferIndex, void* data) override;
private:
	/// <summary>The DirectX-specific graphics settings.</summary>
	DirectXGraphicsSettings* _settings;
	/// <summary>Initializes the graphics settings.</summary>
	void InitializeGraphicsSettings() override;
	/// <summary>Creates a vertex buffer with default settings for the specified data.</summary>
	/// <param name='device'>The current graphics device.</param>
	/// <param name='data'>The data to store in the buffer.</param>
	/// <param name='dataSize'>The size of an individual data element.</param>
	/// <param name='dataCount'>The total elements in the data.</param>
	static ID3D11Buffer* CreateVertexBuffer(ID3D11Device* device, void* data, UINT dataSize, UINT dataCount);
	/// <summary>Creates an index buffer with default settings for the specified data.</summary>
	/// <param name='device'>The current graphics device.</param>
	/// <param name='data'>The data to store in the buffer.</param>
	/// <param name='dataCount'>The total elements in the data.</param>
	static ID3D11Buffer* CreateIndexBuffer(ID3D11Device* device, UINT* data, UINT dataCount);
	static std::vector<char> ReadFileFromStream(std::ifstream* stream);

	std::string _currentPixelShader;
	std::string _currentVertexShader;

	std::map<std::string, ID3D11VertexShader*> _vertexShaders;
	std::map<std::string, ID3D11PixelShader*> _pixelShaders;
	std::map<std::string, ID3D11InputLayout*> _inputLayouts;
	std::map<std::string, ID3D11Buffer*> _vertexShaderConstantBuffers;
	std::map<std::string, ID3D11Buffer*> _pixelShaderConstantBuffers;

	class DirectXInclude;
};

class Graphics::DirectXGraphics::DirectXInclude : public ID3DInclude
{
	// this entire class has been shamelessly stolen from http://nikvoss.com/2013/03/implementing-id3dinclude/
public:
	DirectXInclude(char* shaderDir, char* systemDir);
	HRESULT __stdcall Open(D3D_INCLUDE_TYPE includeType, LPCSTR fileName, LPCVOID parentData, LPCVOID* data, UINT* bytes);
	HRESULT __stdcall Close(LPCVOID data);

private:
	std::string _shaderDir;
	std::string _systemDir;
};