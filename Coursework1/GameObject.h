#pragma once
#include <memory>
#include "Mesh.h"
#include "Transform.h"
#include "Graphics.h"

class GameObject
{
public:
	GameObject();
	~GameObject();

	void Render(Graphics* graphics);
	DirectX::XMMATRIX GetWorldMatrix();

	Transform Transform;
	std::shared_ptr<Mesh> Mesh;
	std::weak_ptr<GameObject> Parent;
	std::vector<std::shared_ptr<GameObject>> Children;
};

