#include "Common.hlsli"

cbuffer Matrices : register (b0)
{
	matrix World;
	matrix WorldViewProjection;
}

VS_OUTPUT main( VS_INPUT input )
{
	VS_OUTPUT output;

	output.Position = mul(float4(input.Position, 1), WorldViewProjection);
	output.Normal = normalize(mul(input.Normal, (float3x3)World));
	output.UvCoordinates = input.UvCoordinates;

	return output;
}