#include <Windows.h>

#pragma once
struct GraphicsSettings
{
public:
	/// <summary>The handle to the window.</summary>
	HWND WindowHandle;
	/// <summary>Finalizes the <see cref='GraphicsSettings' /> instance.</summary> 
	virtual ~GraphicsSettings();
};

