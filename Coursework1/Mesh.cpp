#include "Mesh.h"

Mesh::~Mesh()
{
	this->Vertices.clear();
	this->Normals.clear();
	this->UvCoordinates.clear();
	this->Indices.clear();
}
