#include "DirectXGraphicsSettings.h"
#define SAFE_RELEASE(p) if (p) p->Release(); p = nullptr;

DirectXGraphicsSettings::~DirectXGraphicsSettings()
{
	if (this->DeviceContext) this->DeviceContext->ClearState();

	SAFE_RELEASE(this->Device);
	SAFE_RELEASE(this->SwapChain);
	SAFE_RELEASE(this->RenderTargetView);

}
