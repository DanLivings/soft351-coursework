#pragma once
#include <vector>
#include <DirectXMath.h>
#include "Material.h"

struct Mesh
{
public:
	std::vector<DirectX::XMFLOAT3> Vertices;
	std::vector<DirectX::XMFLOAT3> Normals;
	std::vector<DirectX::XMFLOAT2> UvCoordinates;

	std::vector<unsigned int> Indices;

	Material Material;

	~Mesh();
};

