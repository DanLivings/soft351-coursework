#include "Transform.h"



Transform::Transform()
{
}


Transform::~Transform()
{
}

DirectX::XMMATRIX Transform::GetWorldMatrix()
{
	

	DirectX::XMMATRIX scale = DirectX::XMMatrixScaling(this->Scale.x, this->Scale.y, this->Scale.z);
	
	float xRadians = DirectX::XMConvertToRadians(this->Rotation.x);
	float yRadians = DirectX::XMConvertToRadians(this->Rotation.y);
	float zRadians = DirectX::XMConvertToRadians(this->Rotation.z);
	
	DirectX::XMMATRIX rotate = DirectX::XMMatrixRotationRollPitchYaw(xRadians, yRadians, zRadians);
	DirectX::XMMATRIX translate = DirectX::XMMatrixTranslation(this->Position.x, this->Position.y, this->Position.z);

	DirectX::XMMATRIX result = scale * rotate * translate;

	return result;
}