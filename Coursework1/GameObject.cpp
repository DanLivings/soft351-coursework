#include "GameObject.h"



GameObject::GameObject()
{
}


GameObject::~GameObject()
{
	this->Mesh = nullptr;
}

void GameObject::Render(Graphics* graphics)
{
	graphics->RenderMesh(this->Mesh.get());
}

DirectX::XMMATRIX GameObject::GetWorldMatrix()
{
	DirectX::XMMATRIX worldMatrix;

	if (auto parent = this->Parent.lock())
	{
		DirectX::XMMATRIX parentMatrix = parent->GetWorldMatrix();
		DirectX::XMMATRIX localMatrix = this->Transform.GetWorldMatrix();

		worldMatrix = localMatrix * parentMatrix;
	}
	else
	{
		worldMatrix = this->Transform.GetWorldMatrix();
	}

	return worldMatrix;
}
