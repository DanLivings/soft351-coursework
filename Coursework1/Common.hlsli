/// <summary>Vertex shader input structure.</summary>
struct VS_INPUT
{
	float3 Position : POSITION;
	float3 Normal : NORMAL;
	float2 UvCoordinates : TEXCOORD0;
};

/// <summary>Vertex shader output structure, also used as input to the pixel shader.</summary>
struct VS_OUTPUT
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 UvCoordinates : TEXCOORD0;
};