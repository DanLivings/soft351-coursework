#pragma once
#include <DirectXMath.h>
#include "Texture.h"

struct Material
{
public:
	///<summary>Ambient reflection constant.</summary>
	DirectX::XMFLOAT4 AmbientColour;
	///<summary>Diffuse reflection constant.</summary>
	DirectX::XMFLOAT4 DiffuseColour; 
	///<summary>Specular reflection constant.</summary>
	DirectX::XMFLOAT4 SpecularColour; 

	/// <summary>Glossiness factor; higher values indicate smaller specular reflections.</summary>
	float Glossiness;

	/// <summary>The ambient texture map.</summary>
	Texture* AmbientMap;
	/// <summary>The diffuse texture map.</summary>
	Texture* DiffuseMap;
	/// <summary>The specular texture map.</summary>
	Texture* SpecularMap;
};

