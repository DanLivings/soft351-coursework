#include "Common.hlsli"

cbuffer LightingModel : register (b0)
{
	float4 LightAmbientColour; // Ambient light intensity
	float4 LightDiffuseColour; // Diffuse light intensity
	float4 LightSpecularColour; // Specular light intensity

	float3 LightPosition;

	float3 CameraPosition;

	float4 MaterialAmbientColour; // Ambient reflection constant
	float4 MaterialDiffuseColour; // Diffuse reflection constant
	float4 MaterialSpecularColour; // Specular reflection constant

	float MaterialGlossiness; // Glossiness factor; higher values indicate smaller specular reflections
}

Texture2D AmbientMap : register (t0);
Texture2D DiffuseMap : register (t1);
Texture2D SpecularMap : register (t2);

SamplerState TextureSampler : register(s0);

float4 main(VS_OUTPUT input) : SV_TARGET
{
	float3 light = normalize(LightPosition); //normalize(LightPosition - input.Position);
	float3 normal = normalize(input.Normal);

	float4 diffuse = MaterialDiffuseColour * saturate(dot(light, normal)) * LightDiffuseColour;

	float3 viewer = normalize(CameraPosition); //normalize(CameraPosition - input.Position);
	float3 halfVector = normalize(light + viewer);

	float4 specular = MaterialSpecularColour * pow(saturate(dot(normal, halfVector)), MaterialGlossiness) * LightSpecularColour;

	float4 ambient = MaterialAmbientColour * LightAmbientColour;

	return ambient + diffuse + specular;
}