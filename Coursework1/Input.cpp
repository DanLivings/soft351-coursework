#include "Input.h"

Input::Input()
{
}


Input::~Input()
{
}

bool Input::GetKeyDown(UINT keyCode)
{
	std::map<UINT, bool>::iterator iterator = this->_keysDown.find(keyCode);

	if (iterator != this->_keysDown.end())
	{
		bool wasDown = iterator->second;

		return wasDown;
	}

	return false;
}

bool Input::GetKeyUp(UINT keyCode)
{
	std::map<UINT, bool>::iterator iterator = this->_keysUp.find(keyCode);

	if (iterator != this->_keysUp.end())
	{
		bool wasUp = iterator->second;

		return wasUp;
	}

	return false;
}

bool Input::ProcessKeyboardMessage(MSG message)
{
	if (message.message == WM_KEYDOWN)
	{
		UINT key = message.wParam;

		this->_keysDown[key] = true;

		return true;
	}
	else if (message.message == WM_KEYUP)
	{
		UINT key = message.wParam;

		this->_keysUp[key] = true;

		return true;
	}

	return false;
}

void Input::FlushKeyboardMessages()
{
	this->_keysDown.clear();
	this->_keysUp.clear();
}