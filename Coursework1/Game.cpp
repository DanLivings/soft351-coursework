#include "Game.h"
#include "MeshLoader.h"

typedef struct
{
	DirectX::XMFLOAT4 LightAmbientColour;
	DirectX::XMFLOAT4 LightDiffuseColour;
	DirectX::XMFLOAT4 LightSpecularColour;

	DirectX::XMFLOAT4 LightPosition;

	DirectX::XMFLOAT4 CameraPosition;

	DirectX::XMFLOAT4 MaterialAmbientColour;
	DirectX::XMFLOAT4 MaterialDiffuseColour;
	DirectX::XMFLOAT4 MaterialSpecularColour;

	float MaterialGlossiness;
} LightingModel;

Game::Game(Graphics* graphics, Input* input)
{
	this->_graphics = graphics;
	this->_input = input;

	// load shaders
	graphics->LoadVertexShader("DefaultVertexShader");
	graphics->LoadPixelShader("DefaultPixelShader");
	graphics->SetPixelShader("DefaultPixelShader");
	graphics->SetVertexShader("DefaultVertexShader");
	
	// initialise matrices for vertex buffer
	DirectX::XMMATRIX projection = DirectX::XMMatrixPerspectiveFovLH(DirectX::XM_PIDIV2, 640.0f / 480.0f, 0.01f, 100.0f);

	DirectX::XMVECTOR eye = DirectX::XMVectorSet(2, 2, -2, 0);
	DirectX::XMVECTOR at = DirectX::XMVectorSet(0, 0, 0, 0);
	DirectX::XMVECTOR up = DirectX::XMVectorSet(0, 1, 0, 0);

	DirectX::XMMATRIX view = DirectX::XMMatrixLookAtLH(eye, at, up);

	DirectX::XMMATRIX rotation = DirectX::XMMatrixRotationRollPitchYaw(0, 0, 0);
	DirectX::XMMATRIX scale = DirectX::XMMatrixScaling(1, 1, 1);
	DirectX::XMMATRIX translate = DirectX::XMMatrixTranslation(0, 0, 0);

	DirectX::XMMATRIX world = rotation * scale * translate;

	DirectX::XMMATRIX worldViewProjection = world * view * projection;

	DirectX::XMMATRIX worldTranspose = DirectX::XMMatrixTranspose(world);
	DirectX::XMMATRIX wvpTranspose = DirectX::XMMatrixTranspose(worldViewProjection);
	DirectX::XMMATRIX buf[] = { worldTranspose, wvpTranspose };

	graphics->UpdateConstantBuffer(ShaderType::Vertex, 0, buf);

	// initialise lighting model for pixel buffer
	LightingModel lightingModel =
	{
		DirectX::XMFLOAT4(0.2f, 0.2f, 0.2f, 1),
		DirectX::XMFLOAT4(1, 1, 1, 1),
		DirectX::XMFLOAT4(1, 1, 1, 1),

		DirectX::XMFLOAT4(2, 4, 0, 0),

		DirectX::XMFLOAT4(2, 2, -2, 0),

		DirectX::XMFLOAT4(0.7f, 0.3f, 0, 1),
		DirectX::XMFLOAT4(0.7f, 0.3f, 0, 1),
		DirectX::XMFLOAT4(1, 1, 1, 1),

		80.0f

	};
	 
	graphics->UpdateConstantBuffer(ShaderType::Pixel, 0, &lightingModel);

	// load a mesh
	Mesh* mesh = MeshLoader::CreateFromFile(L".\\Assets\\pig\\pig.obj");

	// create some game objects
	GameObject go;
	go.Mesh = std::make_shared<Mesh>(*mesh);
	go.Transform.Scale = DirectX::XMFLOAT3(0.5, 0.5f, 0.5f);

	this->GameObjects.push_back(std::make_shared<GameObject>(go));

	GameObject go1;
	go1.Mesh = go.Mesh;
	go1.Transform.Scale = DirectX::XMFLOAT3(0.5f, 0.5f, 0.5f);
	go1.Transform.Position = DirectX::XMFLOAT3(2, 0, 0);
	go1.Parent = this->GameObjects[0];

	this->GameObjects.push_back(std::make_shared<GameObject>(go1));

}


Game::~Game()
{
	this->GameObjects.clear();
	delete this->_graphics;
}

void Game::Start()
{
	// Game initialization code here.
}

void Game::Update(float deltaTime)
{
	static float time;
	static DirectX::XMFLOAT3 offset;

	time += deltaTime;

	// Game update code here.

	GameObject* go = this->GameObjects[0].get();

	float y = go->Transform.Rotation.y + 0.5f * 360.0f * deltaTime;

	go->Transform.Rotation.y = y;

	GameObject* go1 = this->GameObjects[1].get();

	if (this->_input->GetKeyDown(0x41)) offset.x--; // A
	if (this->_input->GetKeyDown(0x44)) offset.x++; // D
	if (this->_input->GetKeyDown(0x57)) offset.z++; // W
	if (this->_input->GetKeyDown(0x53)) offset.z--; // S
	
	float x = 2 + offset.x;
	float z = offset.z;

	go1->Transform.Position.x = x;
	go1->Transform.Position.z = z;
}

void Game::OnKeyDown(UINT keyCode)
{
	// Handle a key down event here.
}

void Game::OnKeyUp(UINT keyCode)
{
	// Handle a key up event here.
}

void Game::Render()
{
	this->_graphics->Clear();

	// doing this here isn't a good idea but there's 40 minutes until submission and I don't have time to put it in a Camera class
	DirectX::XMMATRIX projection = DirectX::XMMatrixPerspectiveFovLH(DirectX::XM_PIDIV2, 640.0f / 480.0f, 0.01f, 100.0f);

	DirectX::XMVECTOR eye = DirectX::XMVectorSet(2, 2, -2, 0);
	DirectX::XMVECTOR at = DirectX::XMVectorSet(0, 0, 0, 0);
	DirectX::XMVECTOR up = DirectX::XMVectorSet(0, 1, 0, 0);

	DirectX::XMMATRIX view = DirectX::XMMatrixLookAtLH(eye, at, up);

	int gameObjectCount = this->GameObjects.size();

	// Game rendering code here.
	for (int i = 0; i < gameObjectCount; i++)
	{
		GameObject* gameObject = this->GameObjects[i].get();

		DirectX::XMMATRIX world = gameObject->GetWorldMatrix();
		DirectX::XMMATRIX wvp = world * view * projection;

		DirectX::XMMATRIX worldTranspose = DirectX::XMMatrixTranspose(world);
		DirectX::XMMATRIX wvpTranspose = DirectX::XMMatrixTranspose(wvp);

		DirectX::XMMATRIX buffer[] = { worldTranspose, wvpTranspose };

		this->_graphics->UpdateConstantBuffer(ShaderType::Vertex, 0, &buffer);

		Material material = gameObject->Mesh->Material;

		LightingModel lightingModel =
		{
			DirectX::XMFLOAT4(0.2f, 0.2f, 0.2f, 1),
			DirectX::XMFLOAT4(1, 1, 1, 1),
			DirectX::XMFLOAT4(1, 1, 1, 1),

			DirectX::XMFLOAT4(2, 4, 0, 0),

			DirectX::XMFLOAT4(2, 2, -2, 0),

			material.AmbientColour,
			material.DiffuseColour,
			material.SpecularColour,

			material.Glossiness

		};

		this->_graphics->UpdateConstantBuffer(ShaderType::Pixel, 0, &lightingModel);
			
		gameObject->Render(this->_graphics);
	}

	// Push the rendered frame to screen.
	this->_graphics->Render();
}