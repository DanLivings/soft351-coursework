#pragma once
#include "GraphicsSettings.h"
#include "d3d11.h"

struct DirectXGraphicsSettings :
	public GraphicsSettings
{
public:
	/// <summary>The DirectX driver type.</summary>
	D3D_DRIVER_TYPE DriverType;
	/// <summary>The DirectX feature level.</summary>
	D3D_FEATURE_LEVEL FeatureLevel;
	/// <summary>A pointer to the DirectX device.</summary>
	ID3D11Device* Device;
	/// <summary>A pointer to the DirectX device context.</summary>
	ID3D11DeviceContext* DeviceContext;
	/// <summary>A pointer to the DirectX swap chain.</summary>
	IDXGISwapChain* SwapChain;
	/// <summary>A pointer to the DirectX render-target view.</summary>
	ID3D11RenderTargetView* RenderTargetView;
	/// <summary>A pointer to the depth-stencil buffer.</summary>
	ID3D11Texture2D* DepthStencilBuffer;
	/// <summary>A pointer to the depth-stencil view.</summary>
	ID3D11DepthStencilView* DepthStencilView;
	/// <summary>Finalizes the <see cref='DirectXGraphicsSettings' /> instance.</summary> 
	~DirectXGraphicsSettings();
};

