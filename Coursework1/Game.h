#pragma once
#include <vector>
#include "GameObject.h"
#include "Graphics.h"
#include "Input.h"

class Game
{
public:
	Game(Graphics* graphics, Input* input);
	~Game();

	std::vector<std::shared_ptr<GameObject>> GameObjects;

	void Start();
	void Update(float deltaTime);

	void OnKeyDown(UINT keyCode);
	void OnKeyUp(UINT keyCode);

	void Render();

private:
	Graphics* _graphics;
	Input* _input;
};

