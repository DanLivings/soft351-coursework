#include "Graphics.h"
#include <exception>
#include <DirectXMath.h>
#include <D3Dcompiler.h>
#include <fstream>

#define D3D_COMPILE_STANDARD_FILE_INCLUDE ((ID3DInclude*)(UINT_PTR)1)

Graphics* Graphics::CreateGraphicsObject(HWND windowHandle, GraphicsApi graphicsApi)
{
	Graphics* graphics = nullptr;

	switch (graphicsApi)
	{
	case ApiOpenGL:
		throw std::exception("OpenGL support not implemented");
	case ApiDirectX:
		graphics = new Graphics::DirectXGraphics(windowHandle);
	}

	return graphics;
}

void Graphics::Render()
{
}

void Graphics::InitializeGraphicsSettings()
{
}

Graphics::Graphics()
{
}


Graphics::~Graphics()
{
}

Graphics::DirectXGraphics::DirectXGraphics(HWND windowHandle)
{
	DirectXGraphicsSettings* settings = new DirectXGraphicsSettings;

	settings->WindowHandle = windowHandle;

	this->_settings = settings;

	InitializeGraphicsSettings();
}

void Graphics::DirectXGraphics::InitializeGraphicsSettings()
{
	DirectXGraphicsSettings* settings = this->_settings;

	HRESULT hr = S_OK;

	RECT clientRectangle;
	GetClientRect(settings->WindowHandle, &clientRectangle);
	UINT width = clientRectangle.right - clientRectangle.left;
	UINT height = clientRectangle.bottom - clientRectangle.top;

	UINT createDeviceFlags = 0;
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	UINT driverTypeCount = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT featureLevelCount = ARRAYSIZE(featureLevels);

	DXGI_SWAP_CHAIN_DESC swapChainInfo;
	ZeroMemory(&swapChainInfo, sizeof(swapChainInfo));
	swapChainInfo.BufferCount = 1;
	swapChainInfo.BufferDesc.Width = width;
	swapChainInfo.BufferDesc.Height = height;
	swapChainInfo.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainInfo.BufferDesc.RefreshRate.Numerator = 60;
	swapChainInfo.BufferDesc.RefreshRate.Denominator = 1;
	swapChainInfo.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainInfo.OutputWindow = settings->WindowHandle;
	swapChainInfo.SampleDesc.Count = 1;
	swapChainInfo.SampleDesc.Quality = 0;
	swapChainInfo.Windowed = true;

	for (UINT driverTypeIndex = 0; driverTypeIndex < driverTypeCount; driverTypeIndex++)
	{
		settings->DriverType = driverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(nullptr, settings->DriverType, nullptr, createDeviceFlags, featureLevels, featureLevelCount,
			D3D11_SDK_VERSION, &swapChainInfo, &settings->SwapChain, &settings->Device, &settings->FeatureLevel, &settings->DeviceContext);
		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr)) throw std::exception("Could not create any DirectX device.", hr);

	// Create a render target view
	ID3D11Texture2D* backBuffer = nullptr;
	hr = settings->SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer);
	if (FAILED(hr)) throw std::exception("Could not create a back buffer.", hr);

	hr = settings->Device->CreateRenderTargetView(backBuffer, nullptr, &settings->RenderTargetView);
	backBuffer->Release();
	if (FAILED(hr)) throw std::exception("Could not create a render-target view.", hr);

	settings->DeviceContext->OMSetRenderTargets(1, &settings->RenderTargetView, nullptr);

	// Create a depth stencil buffer
	ID3D11Texture2D* depthTexture = nullptr;
	D3D11_TEXTURE2D_DESC depthTextureDescription;
	depthTextureDescription.Width = width;
	depthTextureDescription.Height = height;
	depthTextureDescription.MipLevels = 1;
	depthTextureDescription.ArraySize = 1;
	depthTextureDescription.Format = DXGI_FORMAT_D16_UNORM;
	depthTextureDescription.SampleDesc.Count = 1;
	depthTextureDescription.SampleDesc.Quality = 0;
	depthTextureDescription.Usage = D3D11_USAGE_DEFAULT;
	depthTextureDescription.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthTextureDescription.CPUAccessFlags = 0;
	depthTextureDescription.MiscFlags = 0;

	hr = settings->Device->CreateTexture2D(&depthTextureDescription, nullptr, &depthTexture);

	if (FAILED(hr)) throw std::exception("Could not create a depth stencil.");

	settings->DepthStencilBuffer = depthTexture;

	// Create the depth stencil state
	D3D11_DEPTH_STENCIL_DESC depthStencilDescription;

	//   Depth test parameters
	depthStencilDescription.DepthEnable = true;
	depthStencilDescription.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDescription.DepthFunc = D3D11_COMPARISON_LESS;

	//   Stencil test parameters
	depthStencilDescription.StencilEnable = true;
	depthStencilDescription.StencilReadMask = 0xFF;
	depthStencilDescription.StencilWriteMask = 0xFF;

	//   Stencil operations if pixel is front-facing
	depthStencilDescription.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDescription.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDescription.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDescription.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	//   Stencil operations if pixel is back-facing
	depthStencilDescription.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDescription.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDescription.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDescription.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	//   Create depth stencil state
	ID3D11DepthStencilState* depthStencilState;
	hr = settings->Device->CreateDepthStencilState(&depthStencilDescription, &depthStencilState);
	
	if (FAILED(hr)) throw std::exception("Could not initialise the depth stencil state.");

	// Bind depth stencil state
	settings->DeviceContext->OMSetDepthStencilState(depthStencilState, 1);

	// Create a depth stencil view
	ID3D11DepthStencilView* depthStencilView = nullptr;
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDescription;
	depthStencilViewDescription.Format = depthTextureDescription.Format;
	depthStencilViewDescription.Flags = 0;
	if (depthTextureDescription.SampleDesc.Count > 1)
	{
		depthStencilViewDescription.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	}
	else
	{
		depthStencilViewDescription.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	}
	depthStencilViewDescription.Texture2D.MipSlice = 0;

	hr = settings->Device->CreateDepthStencilView(depthTexture, &depthStencilViewDescription, &depthStencilView);

	if (FAILED(hr)) throw std::exception("Could not create a depth stencil view.");

	settings->DepthStencilView = depthStencilView;

	settings->DeviceContext->OMSetRenderTargets(1, &settings->RenderTargetView, depthStencilView);

	// Setup the viewport
	D3D11_VIEWPORT viewport;
	viewport.Width = (FLOAT)width;
	viewport.Height = (FLOAT)height;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	settings->DeviceContext->RSSetViewports(1, &viewport);
}

Graphics::DirectXGraphics::~DirectXGraphics()
{
	if (this->_settings) delete this->_settings;
}

void Graphics::DirectXGraphics::Clear()
{
	// Clear the back buffer 
	float backgroundColor[4] = { 0.0f, 0.125f, 0.3f, 1.0f };
	this->_settings->DeviceContext->ClearRenderTargetView(this->_settings->RenderTargetView, backgroundColor);
	this->_settings->DeviceContext->ClearDepthStencilView(this->_settings->DepthStencilView, D3D11_CLEAR_DEPTH, 1.0, 0);
}

void Graphics::DirectXGraphics::LoadPixelShader(std::string shaderName)
{
	std::string fileName = shaderName + ".cso";
	std::ifstream shaderStream = std::ifstream(fileName);
	std::vector<char> shaderData;

	ID3D11PixelShader* shader = nullptr;

	if (!shaderStream)
	{
		fileName = shaderName + ".hlsl";
		shaderStream = std::ifstream(fileName);

		if (!shaderStream) throw std::exception("Could not find the pixel shader.");

		shaderData = Graphics::DirectXGraphics::ReadFileFromStream(&shaderStream);

		ID3DBlob* shaderBlob = nullptr;

		ID3DBlob* errorBlob = nullptr;
		ID3DInclude* include = new DirectXInclude(".\\", "..\\");

		HRESULT result = D3DCompile(&shaderData[0], shaderData.size(), nullptr, nullptr, include, "main", "ps_4_0", D3DCOMPILE_ENABLE_STRICTNESS, 0, &shaderBlob, &errorBlob);

		if (result != S_OK)
		{
			char* error = (char*)errorBlob->GetBufferPointer();
			throw std::exception(error);
		}

		this->_settings->Device->CreatePixelShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), nullptr, &shader);

		if (include) delete include;
		if (shaderBlob) shaderBlob->Release();
		if (errorBlob) errorBlob->Release();
	}
	else
	{
		shaderData = Graphics::DirectXGraphics::ReadFileFromStream(&shaderStream);

		this->_settings->Device->CreatePixelShader(&shaderData[0], shaderData.size(), nullptr, &shader);
	}

	// hard-coded constant buffer description
	// TODO: use reflection to gather this data if time permits
	D3D11_BUFFER_DESC bufferDescription;

	ZeroMemory(&bufferDescription, sizeof(bufferDescription));
	bufferDescription.Usage = D3D11_USAGE_DEFAULT;
	bufferDescription.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bufferDescription.CPUAccessFlags = 0;
	bufferDescription.MiscFlags = 0;
	bufferDescription.ByteWidth = 9 * sizeof(DirectX::XMFLOAT4); //sizeof(DirectX::XMFLOAT4) * 6 + sizeof(DirectX::XMFLOAT3) * 2 + sizeof(float); // ugh

	ID3D11Buffer* buffer;

	HRESULT hr = this->_settings->Device->CreateBuffer(&bufferDescription, nullptr, &buffer);

	if (shader)	this->_pixelShaders[shaderName] = shader;
	if (buffer) this->_pixelShaderConstantBuffers[shaderName] = buffer;
}

void Graphics::DirectXGraphics::LoadVertexShader(std::string shaderName)
{
	std::string fileName = shaderName + ".cso";
	std::ifstream shaderStream = std::ifstream(fileName);
	std::vector<char> shaderData;

	ID3D11InputLayout* inputLayout;

	// hard-coded input layout description
	// TODO: use reflection to gather this information if time permits
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 2, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT elementCount = sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC);

	ID3D11VertexShader* shader = nullptr;

	if (!shaderStream)
	{
		// precompiled shader does not exist, search for an uncompiled version and compile it
		fileName = shaderName + ".hlsl";
		shaderStream = std::ifstream(fileName);

		if (!shaderStream) throw std::exception("Could not find the vertex shader.");

		shaderData = Graphics::DirectXGraphics::ReadFileFromStream(&shaderStream);

		ID3DBlob* shaderBlob = nullptr;

		ID3DBlob* errorBlob = nullptr;

		ID3DInclude* include = new DirectXInclude(".\\", "..\\");

		HRESULT result = D3DCompile(&shaderData[0], shaderData.size(), nullptr, nullptr, include, "main", "vs_4_0", D3DCOMPILE_ENABLE_STRICTNESS, 0, &shaderBlob, &errorBlob);

		if (result != S_OK)
		{
			char* error = (char*)errorBlob->GetBufferPointer();
			throw std::exception(error);
		}

		this->_settings->Device->CreateVertexShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), nullptr, &shader);
		this->_settings->Device->CreateInputLayout(layout, elementCount, shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), &inputLayout);

		if (include) delete include;
		if (shaderBlob) shaderBlob->Release();
		if (errorBlob) errorBlob->Release();
	}
	else
	{
		// is already compiled, just load it directly into a vertex shader object
		shaderData = Graphics::DirectXGraphics::ReadFileFromStream(&shaderStream);

		this->_settings->Device->CreateVertexShader(&shaderData[0], shaderData.size(), nullptr, &shader);
		this->_settings->Device->CreateInputLayout(layout, elementCount, &shaderData[0], shaderData.size(), &inputLayout);

	}

	// hard-coded constant buffer description
	// TODO: use reflection to gather this data if time permits
	D3D11_BUFFER_DESC bufferDescription;

	ZeroMemory(&bufferDescription, sizeof(bufferDescription));
	bufferDescription.Usage = D3D11_USAGE_DEFAULT;
	bufferDescription.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bufferDescription.CPUAccessFlags = 0;
	bufferDescription.MiscFlags = 0;
	bufferDescription.ByteWidth = 2 * sizeof(DirectX::XMMATRIX);

	ID3D11Buffer* buffer;

	this->_settings->Device->CreateBuffer(&bufferDescription, nullptr, &buffer);

	if (shader)	this->_vertexShaders[shaderName] = shader;
	if (inputLayout) this->_inputLayouts[shaderName] = inputLayout;
	if (buffer) this->_vertexShaderConstantBuffers[shaderName] = buffer;
}

void Graphics::DirectXGraphics::Render()
{
	/*// Render a triangle
	// TODO: refactor
	ID3DBlob* vertexShaderBlob = nullptr;
	ID3DBlob* pixelShaderBlob = nullptr;

	char* vertexData = "float4 VS ( float4 pos : POSITION ) : SV_POSITION\n{\n\treturn pos;\n}\n";
	char* pixelData = "float4 PS ( float4 pos : SV_POSITION ) : SV_TARGET\n{\n\treturn float4(1.0f, 1.0f, 0.0f, 1.0f);\n}\n";

	D3DCompile(vertexData, strlen(vertexData), nullptr, nullptr, nullptr, "VS", "vs_4_0", D3DCOMPILE_DEBUG || D3DCOMPILE_ENABLE_STRICTNESS, 0, &vertexShaderBlob, nullptr);
	D3DCompile(pixelData, strlen(pixelData), nullptr, nullptr, nullptr, "PS", "ps_4_0", D3DCOMPILE_DEBUG || D3DCOMPILE_ENABLE_STRICTNESS, 0, &pixelShaderBlob, nullptr);

	ID3D11VertexShader* vertexShader = nullptr;
	ID3D11PixelShader* pixelShader = nullptr;

	this->_settings->Device->CreateVertexShader(vertexShaderBlob->GetBufferPointer(), vertexShaderBlob->GetBufferSize(), nullptr, &vertexShader);
	this->_settings->Device->CreatePixelShader(pixelShaderBlob->GetBufferPointer(), pixelShaderBlob->GetBufferSize(), nullptr, &pixelShader);

	this->_settings->DeviceContext->VSSetShader(vertexShader, nullptr, 0);
	this->_settings->DeviceContext->PSSetShader(pixelShader, nullptr, 0);

	ID3D11InputLayout* inputLayout;

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 2, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT elementCount = sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC);
	this->_settings->Device->CreateInputLayout(layout, elementCount, vertexShaderBlob->GetBufferPointer(), vertexShaderBlob->GetBufferSize(), &inputLayout);

	this->_settings->DeviceContext->IASetInputLayout(inputLayout);


	DirectX::XMFLOAT3 vertices[] =
	{
		DirectX::XMFLOAT3(0.0f, 0.5f, 0.5f),
		DirectX::XMFLOAT3(0.5f, -0.5f, 0.5f),
		DirectX::XMFLOAT3(-0.5f, -0.5f, 0.5f),
	};

	ID3D11Buffer* vertexBuffer;

	D3D11_BUFFER_DESC bufferDescription;

	ZeroMemory(&bufferDescription, sizeof(bufferDescription));
	bufferDescription.Usage = D3D11_USAGE_DEFAULT;
	bufferDescription.ByteWidth = sizeof(DirectX::XMFLOAT3) * 3;
	bufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDescription.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA initialData;
	ZeroMemory(&initialData, sizeof(DirectX::XMFLOAT3));
	initialData.pSysMem = vertices;

	this->_settings->Device->CreateBuffer(&bufferDescription, &initialData, &vertexBuffer);

	UINT stride = sizeof(DirectX::XMFLOAT3);
	UINT offset = 0;

	this->_settings->DeviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	this->_settings->DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	this->_settings->DeviceContext->Draw(3, 0);

	vertexShaderBlob->Release();
	pixelShaderBlob->Release();
	vertexShader->Release();
	pixelShader->Release();
	inputLayout->Release();
	vertexBuffer->Release();
	// end triangles*/

	// Present the information rendered to the back buffer to the front buffer (the screen)
	this->_settings->SwapChain->Present(0, 0);
}



void Graphics::DirectXGraphics::RenderMesh(Mesh* mesh)
{
	if (!mesh) throw std::exception("The mesh is null and cannot be rendered.");

	if (mesh->Indices.size() == 0 || mesh->Normals.size() == 0 || mesh->UvCoordinates.size() == 0 || mesh->Vertices.size() == 0)
	{
		throw std::exception("The mesh is invalid and cannot be rendered.");
	}

	const UINT float3size = sizeof(DirectX::XMFLOAT3);
	const UINT float2size = sizeof(DirectX::XMFLOAT2);

	ID3D11Buffer* positions = CreateVertexBuffer(this->_settings->Device, &mesh->Vertices[0], float3size, mesh->Vertices.size());
	ID3D11Buffer* normals = CreateVertexBuffer(this->_settings->Device, &mesh->Normals[0], float3size, mesh->Normals.size());
	ID3D11Buffer* uvCoords = CreateVertexBuffer(this->_settings->Device, &mesh->UvCoordinates[0], float2size, mesh->UvCoordinates.size());

	ID3D11Buffer* buffers[3] = { positions, normals, uvCoords };
	UINT strides[3] = { float3size, float3size, float2size };
	UINT offsets[3] = { 0, 0, 0 };

	ID3D11Buffer* indices = CreateIndexBuffer(this->_settings->Device, &mesh->Indices[0], mesh->Indices.size());

	this->_settings->DeviceContext->IASetVertexBuffers(0, 3, buffers, strides, offsets);
	this->_settings->DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	this->_settings->DeviceContext->IASetIndexBuffer(indices, DXGI_FORMAT_R32_UINT, 0);

	this->_settings->DeviceContext->DrawIndexed(mesh->Indices.size(), 0, 0);

	positions->Release();
	normals->Release();
	uvCoords->Release();
	indices->Release();
}

void Graphics::DirectXGraphics::SetTextureMap(UINT textureIndex, Texture* texture)
{
	/*if (texture != nullptr)
	{

	}*/

	return;
	// couldn't figure out how to load an image into memory and then into a DirectX-specific
	// format that could be implemented in time, but the images are loaded in memory
}

void Graphics::DirectXGraphics::SetProjectionMatrix(DirectX::XMMATRIX projectionMatrix)
{
	_projectionMatrix = projectionMatrix;
}

void Graphics::DirectXGraphics::SetViewMatrix(DirectX::XMMATRIX viewMatrix)
{
	_viewMatrix = viewMatrix;
}

void Graphics::DirectXGraphics::SetWorldMatrix(DirectX::XMMATRIX worldMatrix)
{
	_worldMatrix = worldMatrix;
}

void Graphics::DirectXGraphics::SetPixelShader(std::string shaderName)
{
	std::map<std::string, ID3D11PixelShader*>::iterator iter = this->_pixelShaders.find(shaderName);
	
	ID3D11PixelShader* shader;

	if (iter != this->_pixelShaders.end())
	{
		shader = iter->second;

		this->_settings->DeviceContext->PSSetShader(shader, nullptr, 0);
		this->_currentPixelShader = shaderName;
	}
	else throw std::exception("Pixel shader has not been loaded.");
}

void Graphics::DirectXGraphics::SetVertexShader(std::string shaderName)
{
	std::map<std::string, ID3D11VertexShader*>::iterator shaderIterator = this->_vertexShaders.find(shaderName);

	ID3D11VertexShader* shader;
	ID3D11InputLayout* inputLayout;

	if (shaderIterator != this->_vertexShaders.end())
	{
		shader = shaderIterator->second;

		std::map<std::string, ID3D11InputLayout*>::iterator inputLayoutIterator = this->_inputLayouts.find(shaderName);

		if (inputLayoutIterator != this->_inputLayouts.end())
		{
			inputLayout = inputLayoutIterator->second;

			this->_settings->DeviceContext->VSSetShader(shader, nullptr, 0);
			this->_settings->DeviceContext->IASetInputLayout(inputLayout);
			this->_currentVertexShader = shaderName;
		}
		else throw std::exception("Could not find an input layout for the vertex shader.");
	}
	else throw std::exception("Vertex shader has not been loaded.");
}

void Graphics::DirectXGraphics::UpdateConstantBuffer(ShaderType shaderType, UINT bufferIndex, void* data)
{
	ID3D11Buffer* buffer;
	std::map<std::string, ID3D11Buffer*>::iterator iterator;

	switch (shaderType)
	{
	case ShaderType::Vertex:
		iterator = this->_vertexShaderConstantBuffers.find(this->_currentVertexShader);

		if (iterator != this->_vertexShaderConstantBuffers.end())
		{
			buffer = iterator->second;

			this->_settings->DeviceContext->UpdateSubresource(buffer, 0, nullptr, data, 0, 0);
			this->_settings->DeviceContext->VSSetConstantBuffers(0, 1, &buffer);
		}

		break;
	case ShaderType::Pixel:
		iterator = this->_pixelShaderConstantBuffers.find(this->_currentPixelShader);

		if (iterator != this->_pixelShaderConstantBuffers.end())
		{
			buffer = iterator->second;

			this->_settings->DeviceContext->UpdateSubresource(buffer, 0, nullptr, data, 0, 0);
			this->_settings->DeviceContext->PSSetConstantBuffers(0, 1, &buffer);
		}

		break;
	default:
		throw std::exception("Unknown shader type specified.");
	}
}

ID3D11Buffer* Graphics::DirectXGraphics::CreateVertexBuffer(ID3D11Device* device, void* data, UINT dataSize, UINT dataCount)
{
	ID3D11Buffer* buffer = nullptr;

	D3D11_BUFFER_DESC bufferDescription;

	ZeroMemory(&bufferDescription, sizeof(bufferDescription));
	bufferDescription.Usage = D3D11_USAGE_DEFAULT;
	bufferDescription.ByteWidth = dataSize * dataCount;
	bufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDescription.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA initialData;
	ZeroMemory(&initialData, dataSize);
	initialData.pSysMem = data;

	device->CreateBuffer(&bufferDescription, &initialData, &buffer);

	return buffer;
}

ID3D11Buffer* Graphics::DirectXGraphics::CreateIndexBuffer(ID3D11Device* device, UINT* data, UINT dataCount)
{
	const UINT dataSize = sizeof(UINT);
	ID3D11Buffer* buffer = nullptr;

	D3D11_BUFFER_DESC bufferDescription;

	ZeroMemory(&bufferDescription, sizeof(bufferDescription));
	bufferDescription.Usage = D3D11_USAGE_DEFAULT;
	bufferDescription.ByteWidth = dataSize * dataCount;
	bufferDescription.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDescription.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA initialData;
	ZeroMemory(&initialData, dataSize);
	initialData.pSysMem = data;

	device->CreateBuffer(&bufferDescription, &initialData, &buffer);

	return buffer;
}

std::vector<char> Graphics::DirectXGraphics::ReadFileFromStream(std::ifstream* stream)
{
	// get file size
	stream->seekg(0, std::ios::end);
	std::streampos length = stream->tellg();
	stream->seekg(0, std::ios::beg);

	// read the file
	std::vector<char> data = std::vector<char>(length);
	stream->read(&data[0], length);

	return data;
}

Graphics::DirectXGraphics::DirectXInclude::DirectXInclude(char* shaderDir, char* systemDir)
{
	_shaderDir = std::string(shaderDir);
	_systemDir = std::string(systemDir);
}

HRESULT __stdcall Graphics::DirectXGraphics::DirectXInclude::Open(D3D_INCLUDE_TYPE includeType, LPCSTR fileName, LPCVOID parentData, LPCVOID* data, UINT* bytes)
{
	try {
		std::string finalPath;
		switch (includeType) {
		case D3D_INCLUDE_LOCAL:
			finalPath = _shaderDir + "\\" + fileName;
				break;
		case D3D_INCLUDE_SYSTEM:
			finalPath = _systemDir + "\\" + fileName;
				break;
		default:
			assert(0);
		}

		std::ifstream includeFile(finalPath.c_str(), std::ios::in | std::ios::binary | std::ios::ate);

		if (includeFile.is_open()) {
			long long fileSize = includeFile.tellg();
			char* buf = new char[fileSize];
			includeFile.seekg(0, std::ios::beg);
			includeFile.read(buf, fileSize);
			includeFile.close();
			*data = buf;
			*bytes = fileSize;
		}
		else {
			return E_FAIL;
		}
		return S_OK;
	}
	catch (std::exception& e) {
		return E_FAIL;
	}
}

HRESULT __stdcall Graphics::DirectXGraphics::DirectXInclude::Close(LPCVOID data)
{
	char* buf = (char*)data;
	delete[] buf;
	return S_OK;
}