#pragma once
#include "Mesh.h"
#include <map>
#include <string>

class MeshLoader
{
public:
	/// <summary>Creates a <see cref="Mesh" /> from the specified file.</summary>
	/// <param name='fileName'>The location of the file to open.</param>
	static Mesh* CreateFromFile(std::wstring fileName);
private:
	/// <summary>Enumerates the mesh formats that can be opened.</summary>
	enum MeshType
	{
		Unknown,
		SdkMesh,
		WavefrontObj
	};
	typedef std::map<std::wstring, MeshType> MeshFormatMap;
	static MeshFormatMap _meshFormatMap;
	static MeshFormatMap CreateMeshFormatMap();

	/// <summary>Opens a Wavefront OBJ file.</summary>
	/// <param name='fileName'>The location of the file to open.</param>
	static Mesh* LoadWavefront(std::wstring fileName);
	/// <summary>Loads a companion MTL file for a Wavefront OBJ.</summary>
	/// <param name='materialLibrary'>The material library to append to.</param>
	/// <param name='fileName'>The location of the file to open.</param>
	static void LoadMaterial(std::map<std::wstring, Material>* materialLibrary, std::wstring fileName);
	/// <summary>Loads a texture for a material.</summary>
	/// <param name='fileName'>The location of the file to open.</param>
	static Texture* LoadTexture(std::wstring fileName);
};
