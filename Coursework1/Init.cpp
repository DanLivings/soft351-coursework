#include <Windows.h>
#include <exception>
#include "Graphics.h"
#include "Game.h"
#include "Input.h"
#include <chrono>

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

HWND InitializeWindow(HINSTANCE, int);
LRESULT CALLBACK ProcessWindowMessage(HWND, UINT, WPARAM, LPARAM);

/// <summary>The main entry point of the program.</summary>
/// <param name='instanceHandle'>The handle of the current program instance.</param>
/// <param name='previousInstanceHandle'>(Obsolete) The handle of the previous program instance.</param>
/// <param name='commandLineArgs'>The command line arguments, excluding the program file name.</param>
/// <param name='showWindowCommand'>A parameter that controls how the window is to be shown.</param>
/// <returns><c>0</c> if the program quit normally, or a negative value otherwise.</returns>
int WINAPI wWinMain(HINSTANCE instanceHandle, HINSTANCE previousInstanceHandle, LPWSTR commandLineArgs, int showWindowCommand)
{
#ifdef _CRTDBG_MAP_ALLOC
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif // _CRTDBG_MAP_ALLOC

	MSG message = { 0 };

	try
	{
		HWND windowHandle = InitializeWindow(instanceHandle, showWindowCommand);
		Graphics* graphics = Graphics::CreateGraphicsObject(windowHandle, GraphicsApi::ApiDirectX);
		Input input;
		Game game = Game(graphics, &input);

		std::chrono::time_point<std::chrono::system_clock> lastFrame, currentFrame;

		lastFrame = currentFrame = std::chrono::system_clock::now();

		while (WM_QUIT != message.message)
		{
			if (PeekMessage(&message, nullptr, 0, 0, PM_REMOVE))
			{
				bool handled = false;

				if (message.message >= WM_KEYFIRST && message.message <= WM_KEYLAST)
				{
					handled = input.ProcessKeyboardMessage(message);
				}

				if (!handled)
				{
					TranslateMessage(&message);
					DispatchMessage(&message);
				}
			}
			else
			{
				lastFrame = currentFrame;
				currentFrame = std::chrono::system_clock::now();

				std::chrono::duration<float> duration = currentFrame - lastFrame;
				game.Update(duration.count());
				game.Render();
				input.FlushKeyboardMessages();
			}
		}
	}
	catch (const std::exception& ex)
	{
		const char* errorMessage = ex.what();
		wchar_t* message;
		int size = MultiByteToWideChar(CP_ACP, 0, errorMessage, -1, nullptr, 0);
		message = new wchar_t[size];
		MultiByteToWideChar(CP_ACP, 0, errorMessage, -1, message, size);

		MessageBox(nullptr, message, L"Error", MB_ICONERROR | MB_OK);
		return -1;
	}

#ifdef _CRTDBG_MAP_ALLOC
	_CrtDumpMemoryLeaks();
#endif // _CRTDBG_MAP_ALLOC

	return message.wParam;
}

/// <summary>Initializes the main window of the application.</summary>
/// <param name='instanceHandle'>The handle to the instance of the program.</param>
/// <param name='showWindowCommand'>A parameter that controls how the window is to be shown.</param>
/// <returns>A handle to the window.</returns>
HWND InitializeWindow(HINSTANCE instanceHandle, int showWindowCommand)
{
	WNDCLASSEX windowAttributes;
	windowAttributes.cbSize = sizeof(WNDCLASSEX);
	windowAttributes.style = CS_HREDRAW | CS_VREDRAW;
	windowAttributes.lpfnWndProc = ProcessWindowMessage;
	windowAttributes.cbClsExtra = 0;
	windowAttributes.cbWndExtra = 0;
	windowAttributes.hInstance = instanceHandle;
	windowAttributes.hIcon = LoadIcon(instanceHandle, nullptr); //(LPCTSTR)IDI_TUTORIAL1);
	windowAttributes.hCursor = LoadCursor(nullptr, IDC_ARROW);
	windowAttributes.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	windowAttributes.lpszMenuName = nullptr;
	windowAttributes.lpszClassName = L"MainWindowClass";
	windowAttributes.hIconSm = LoadIcon(windowAttributes.hInstance, nullptr); //(LPCTSTR)IDI_TUTORIAL1);

	if (!RegisterClassEx(&windowAttributes)) throw std::exception("Could not register window class.");

	RECT windowRectangle = { 0, 0, 640, 480 };
	AdjustWindowRect(&windowRectangle, WS_OVERLAPPEDWINDOW, FALSE);

	HWND windowHandle = CreateWindow(L"MainWindowClass",
									 L"SOFT351 - Coursework 1",
									 WS_OVERLAPPEDWINDOW,
									 CW_USEDEFAULT,
									 CW_USEDEFAULT,
									 windowRectangle.right - windowRectangle.left,
									 windowRectangle.bottom - windowRectangle.top,
									 nullptr,
									 nullptr,
									 instanceHandle,
									 nullptr);

	if (!windowHandle) throw std::exception("Could not create window handle.");

	ShowWindow(windowHandle, showWindowCommand);

	return windowHandle;
}

/// <summary>Processes the window message queue.</summary>
/// <param name='windowHandle'>The handle to the window.</param>
/// <param name='message'>The message to process.</param>
/// <param name='wParameters'>Additional word-length parameters for the message.</param>
/// <param name='lParameters'>Additional long parameters for the message.</param>
LRESULT CALLBACK ProcessWindowMessage(HWND windowHandle, UINT message, WPARAM wParameters, LPARAM lParameters)
{
	PAINTSTRUCT paintInfo;
	HDC deviceContextHandle;

	switch (message)
	{
	case WM_PAINT:
		deviceContextHandle = BeginPaint(windowHandle, &paintInfo);
		EndPaint(windowHandle, &paintInfo);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(windowHandle, message, wParameters, lParameters);
	}
}