#include "MeshLoader.h"
#include <fstream>
#include <Shlwapi.h>
#include <ctype.h>
#include <sstream>
#include <DirectXMath.h>


MeshLoader::MeshFormatMap MeshLoader::_meshFormatMap = MeshLoader::CreateMeshFormatMap();

Mesh* MeshLoader::CreateFromFile(std::wstring fileName)
{
	std::wstring extension = PathFindExtension(fileName.c_str());
	
	std::wstring extensionLower;

	for (int i = 0; i < extension.length(); i++)
	{
		extensionLower.push_back((wchar_t)towlower(extension[i]));
	}

	MeshType meshType = _meshFormatMap[extensionLower];

	switch (meshType)
	{
	case MeshType::SdkMesh:
		throw std::exception("SDKMESH mesh importing not implemented.");
	case MeshType::WavefrontObj:
		//throw std::exception("OBJ mesh importing not implemented.");
		return LoadWavefront(fileName);
	default:
		throw std::exception("Could not create mesh from unknown file type.");
	}

	return nullptr;

	//return IMesh();
}

MeshLoader::MeshFormatMap MeshLoader::CreateMeshFormatMap()
{
	MeshLoader::MeshFormatMap map;

	map[L".sdkmesh"] = MeshType::SdkMesh;
	map[L".obj"] = MeshType::WavefrontObj;

	return map;
}

Mesh* MeshLoader::LoadWavefront(std::wstring fileName)
{
	Mesh* result = new Mesh;

	struct Vertex
	{
		DirectX::XMFLOAT3 Position;
		DirectX::XMFLOAT3 Normal;
		DirectX::XMFLOAT2 UvCoordinates;

		bool operator==(const Vertex &other) const
		{
			return this->Position.x == other.Position.x
				&& this->Position.y == other.Position.y
				&& this->Position.z == other.Position.z
				&& this->Normal.x == other.Normal.x
				&& this->Normal.y == other.Normal.y
				&& this->Normal.z == other.Normal.z
				&& this->UvCoordinates.x == other.UvCoordinates.x
				&& this->UvCoordinates.y == other.UvCoordinates.y;
		};

		bool operator<(const Vertex &other) const
		{
			return this->Position.x < other.Position.x
				|| this->Position.y < other.Position.y
				|| this->Position.z < other.Position.z
				|| this->Normal.x < other.Normal.x
				|| this->Normal.y < other.Normal.y
				|| this->Normal.z < other.Normal.z
				|| this->UvCoordinates.x < other.UvCoordinates.x
				|| this->UvCoordinates.y < other.UvCoordinates.y;
		};
	};

	std::map<std::wstring, Material> materialLibrary;

	std::map<Vertex, UINT> indexMap;

	std::vector<DirectX::XMFLOAT3> vertices;
	std::vector<DirectX::XMFLOAT3> normals;
	std::vector<DirectX::XMFLOAT2> coords;

	std::wifstream fileStream = std::wifstream(fileName);
	std::wstring line;

	int bufferIndex = 0;

	while (std::getline(fileStream, line))
	{
		std::wistringstream tokenStream = std::wistringstream(line);

		std::wstring command;

		tokenStream >> command;

		if (command == L"mtllib")
		{
			// load material definitions
			std::wstring materialFile;

			tokenStream >> materialFile;

			// assumes the file path is a relative path
			// this is a bit of a naughty hack, if I had time
			// I'd use a proper path resolving library
			int index = fileName.find_last_of(L'\\');

			std::wstring directory = fileName.substr(0, index + 1);
			
			LoadMaterial(&materialLibrary, directory + materialFile);
		}
		else if (command == L"usemtl")
		{
			std::wstring materialName;

			tokenStream >> materialName;

			std::map<std::wstring, Material>::iterator iterator = materialLibrary.find(materialName);

			if (iterator != materialLibrary.end())
			{
				Material material = iterator->second;

				result->Material = material;
			}
			else throw std::exception("Could not find the material in the library.");
		}
		else if (command == L"v")
		{
			//load vertex

			float x, y, z;

			tokenStream >> x >> y >> z;

			DirectX::XMFLOAT3 vertex = DirectX::XMFLOAT3(x, y, z);

			vertices.push_back(vertex);
		}
		else if (command == L"vt")
		{
			//load texture vertex

			float u, v;

			tokenStream >> u >> v;

			DirectX::XMFLOAT2 coord = DirectX::XMFLOAT2(u, v);

			coords.push_back(coord);
		}
		else if (command == L"vn")
		{
			//load vertex normal

			float x, y, z;

			tokenStream >> x >> y >> z;

			DirectX::XMFLOAT3 normal = DirectX::XMFLOAT3(x, y, z);

			normals.push_back(normal);
		}
		else if (command == L"f")
		{
			//load face

			std::wstring vertex;

			for (int v = 0; v < 3; v++)
			{

				tokenStream >> vertex;

				std::wistringstream vertexComponentStream = std::wistringstream(vertex);
				std::wstring component;

				Vertex vertexData;

				// extract the indices from the "i1/i2/i3" index notation 
				// TODO: optimise so that identical position-UV-normal tuples are only stored once
				for (int i = 0; std::getline(vertexComponentStream, component, L'/'); i++)
				{
					bool valueGiven;
					int value;
					if (component == L"")
					{
						value = -1;
						valueGiven = false;
					}
					else
					{
						value = std::stoi(component) - 1; // Wavefront uses 1-based indexing

						valueGiven = true;
					}

					if (i == 0)
					{
						// a vertex should always be given
						if (!valueGiven || value >= vertices.size()) throw std::exception("Undefined vertex");

						// a negative index counts backwards from the end of the list, i.e. -1 is the last element
						if (value < 0) value = vertices.size() + value;

						//result->Vertices.push_back(vertices[value]); 
						vertexData.Position = vertices[value];

					}
					else if (i == 1)
					{
						// if a UV coordinate isn't given, just give a default
						if (!valueGiven)
						{
							result->UvCoordinates.push_back(DirectX::XMFLOAT2(0, 0));
						}
						else
						{
							if (value >= coords.size()) throw std::exception("Undefined UV coordinate");

							// a negative index counts backwards from the end of the list, i.e. -1 is the last element
							if (value < 0) value = coords.size() + value;

							//result->UvCoordinates.push_back(coords[value]);
							vertexData.UvCoordinates = coords[value];
						}
					}
					else if (i == 2)
					{
						// if a normal isn't supplied, just give a default
						// TODO: make this the actual normal calculation
						if (!valueGiven)
						{
							result->Normals.push_back(DirectX::XMFLOAT3(0, 0, 0));
						}
						else
						{
							if (value >= normals.size()) throw std::exception("Undefined normal vector");

							// a negative index counts backwards from the end of the list, i.e. -1 is the last element
							if (value < 0) value = normals.size() + value;

							//result->Normals.push_back(normals[value]);
							vertexData.Normal = normals[value];
						}
					}
				}

				std::map<Vertex, UINT>::iterator vertexIterator = indexMap.find(vertexData);

				if (vertexIterator != indexMap.end())
				{
					UINT index = vertexIterator->second;

					result->Indices.push_back(index);
				}
				else
				{
					result->Vertices.push_back(vertexData.Position);
					result->Normals.push_back(vertexData.Normal);
					result->UvCoordinates.push_back(vertexData.UvCoordinates);

					result->Indices.push_back(bufferIndex);

					bufferIndex++;
				}

				//result->Indices.push_back(bufferIndex);


			}
		}
		else
		{
			continue;
		}
	}

	if (fileStream.bad()) throw std::exception("Could not load mesh file.");

	return result;
}

void MeshLoader::LoadMaterial(std::map<std::wstring, Material>* materialLibrary, std::wstring fileName)
{
	bool startedMaterial = false;
	Material material;
	std::wstring materialName;

	std::wifstream fileStream = std::wifstream(fileName);
	std::wstring line;

	while (std::getline(fileStream, line))
	{
		std::wistringstream tokenStream = std::wistringstream(line);

		std::wstring command;

		tokenStream >> command;

		if (command == L"newmtl")
		{
			if (startedMaterial)
			{
				// commit the previous material to the library
				(*materialLibrary)[materialName] = material;
			}

			// start a new material
			material = Material();
			tokenStream >> materialName;
			startedMaterial = true;
		}
		else if (command == L"Ka")
		{
			// define ambient colour
			float r, g, b;
			tokenStream >> r >> g >> b;

			material.AmbientColour = DirectX::XMFLOAT4(r, g, b, 1);
		}
		else if (command == L"Kd")
		{
			// define diffuse colour
			float r, g, b;
			tokenStream >> r >> g >> b;

			material.DiffuseColour = DirectX::XMFLOAT4(r, g, b, 1);
		}
		else if (command == L"Ks")
		{
			// define specular colour
			float r, g, b;
			tokenStream >> r >> g >> b;

			material.SpecularColour = DirectX::XMFLOAT4(r, g, b, 1);
		}
		else if (command == L"Ns")
		{
			// define specular exponent
			float n;
			tokenStream >> n;

			material.Glossiness = n;
		}
		else if (command == L"d")
		{
			// define transparency (1 is opaque)

			float d;
			tokenStream >> d;

			material.AmbientColour.w = d;
			material.DiffuseColour.w = d;
			material.SpecularColour.w = d;
		}
		else if (command == L"Tr")
		{
			// define transparency (0 is opaque)

			float t, d;
			tokenStream >> t;

			d = 1 - t;

			material.AmbientColour.w = d;
			material.DiffuseColour.w = d;
			material.SpecularColour.w = d;
		}
		else if (command == L"map_Ka")
		{
			std::wstring textureFile;

			tokenStream >> textureFile;

			// copied from above, which is yet another naughty hack but this is due in two hours 

			// assumes the file path is a relative path
			// this is a bit of a naughty hack, if I had time
			// I'd use a proper path resolving library
			int index = fileName.find_last_of(L'\\');

			std::wstring directory = fileName.substr(0, index + 1);

			material.AmbientMap = LoadTexture(directory + textureFile);
		}
		else if (command == L"map_Kd")
		{
			std::wstring textureFile;

			tokenStream >> textureFile;

			// copied from above, which is yet another naughty hack but this is due in two hours 

			// assumes the file path is a relative path
			// this is a bit of a naughty hack, if I had time
			// I'd use a proper path resolving library
			int index = fileName.find_last_of(L'\\');

			std::wstring directory = fileName.substr(0, index + 1);

			material.DiffuseMap = LoadTexture(directory + textureFile);
		}
		else if (command == L"map_Ks")
		{
			std::wstring textureFile;

			tokenStream >> textureFile;

			// copied from above, which is yet another naughty hack but this is due in two hours 

			// assumes the file path is a relative path
			// this is a bit of a naughty hack, if I had time
			// I'd use a proper path resolving library
			int index = fileName.find_last_of(L'\\');

			std::wstring directory = fileName.substr(0, index + 1);

			material.SpecularMap = LoadTexture(directory + textureFile);
		}
		else
		{
			continue;
		}

	}

	if (startedMaterial) (*materialLibrary)[materialName] = material;

	if (fileStream.bad()) throw std::exception("Could not load material.");
}

Texture* MeshLoader::LoadTexture(std::wstring fileName)
{
	std::ifstream file = std::ifstream(fileName, std::ios::binary | std::ios::ate);

	if (!file.is_open()) throw std::exception("Could not load texture.");

	std::ifstream::pos_type position = file.tellg();

	std::vector<char> bytes = std::vector<char>(position);

	file.seekg(0, std::ios::beg);
	file.read(&bytes[0], position);

	Texture* result = new Texture();

	result->Bytes = bytes;

	return result;
}