#pragma once
#include <DirectXMath.h>

class Transform
{
public:
	Transform();
	~Transform();

	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT3 Scale;
	DirectX::XMFLOAT3 Rotation;

	DirectX::XMMATRIX GetWorldMatrix();
};

